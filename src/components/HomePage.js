import React, { Component } from 'react'
import MasterForm from './MasterForm';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import validator from 'validator';
import DateFnsUtils from '@date-io/date-fns';
import { et } from 'date-fns/locale';

const createData = (name, currentRate, rateWithPulse, pricePromiseRate) => ({
    id: name.replace(" ", "_"),
    name,
    currentRate,
    rateWithPulse,
    pricePromiseRate,
    isEditMode: true
  });
  const createGasData = (name, naturalGas, lpgRate) => ({
    id: name.replace(" ", "_"),
    name,
    naturalGas,
    lpgRate,
    isEditMode: true
  });
export default class HomePage extends Component {
    constructor (props) {
        super(props)
        this.state ={
            loading:false,
            title:'',
            firstName:'',
            middleName:'',
            lastName:'',
            accountName: '',
            dateOfBirth:new Date(),
            newDateOfBirth:'',
            mobilePhone:'',
            homePhone:'',
            workPhone:'',
            email:'',
            confirmEmail:'',
            isAccountHolder:'Yes',
            identificationType:'Driver Licence',
            license5a:'',
            license5b:'',
            passportNo:'',
            passportCountry:'',
            passportExpiry:new Date(),
            newPassportExpiry:'',
            onlineOrPost:'online',
            onlineEmail:'',
            authorisedContactTitle:'',
            authorisedFirstName:'',
            authorisedLastName:'',
            authorisedDateOfBirth:new Date(),
            newAuthorisedDateOfBirth:'',
            billedByAnotherCompany:'Yes',
            customerSituation:'Moving to a new house',
            dateOfMoveIn:new Date(),
            newDateOfMoveIn:'',
            accessNote:'',
            unitOrStNo:'',
            streetAddress:'',
            suburb:'',
            townOrCity:'',
            postcode:'',
            unitOrStNoPost:'',
            streetAddressPost:'',
            suburbPost:'',
            townOrCityPost:'',
            postcodePost:'',
            ICP:'',
            medicallyDependant:'',
            otherMedicallyDependant:'',
            paymentMethod:'Other',
            signatureURL:'',
            gasICP:'',
            interestInSolar:'No',
            eMeterNotes:'',
            gasMeterNotes:'',
            selectedServices:{electricity: true, gas:false,lpg:false,broadband:false},
            eMeterLocation:[],
            gasMeterLocation:[],
            electricityRates:[
                createData("Fixed (Daily)",0, 0, 0),
                createData("Variable meter 1",0, 0, 0),
                createData("Variable meter 2",0, 0, 0),
                createData("Variable meter 3",0, 0, 0),
                createData("Competitor PPD",0, "-", "-"),
                createData("Price Promise","-", 0, 0)
                
              ],
            gasRates:[
                createGasData("Fixed (Daily)",0, 0, 0),
                createGasData("Variable",0, 0, 0),
                createGasData("All Energy Discount",0, 0, 0)
              ],
            representativeName:'',
            electricityUserType:'',
            gasUserType:'',
            noticeToCustomer:'You can cancel or withdraw from this Agreement within 5 working days of signing this Agreement, by contacting us.',
            lpgService:"",
            currentYear: new Date().getFullYear(),
            sameAddress: false,
            sameEmail: false,
            comments:'',
            company:'Focus',
            broadbandPlans: [],
            broadbandPlan:[{
                plan:'',
                connection:'',
                price:''
            }
            ],
            loadingBroadband: true,
            desiredPlanDetails: '',
            broadbandDetails:[{
                AccountNumber: '',
                AccountName: '',
                currentBroadbandProvider: '',
                LandlineNumber: '',
                currentLandlineProvider: '',
                landlineAccountNumber: '',
                landlineAccountName: '',
                haveISP: 'Yes',
                sameAccountName: false,
                haveJackpoints: false,
                haveFibre: 'Yes',
                getLandline: 'No',
                haveLandline: 'Yes',
                sameLandlineISP: 'Yes',
            }],
            confirmPhonePlan:[{
                title:'',
                details:'',
                price:''
            }
            ],
            phonePlans:'',
            terms:'',
            broadbandTerms:'',
            
        }
    }
    
    setBroadbandInfo = input =>{
        this.setState({broadbandDetails: [input]})
    }
    setPhonePlan = (phonePlanFromChild)=>{
            if(phonePlanFromChild instanceof Array){
                this.setState({
                    confirmPhonePlan: phonePlanFromChild
                })
            }else{
                this.setState({confirmPhonePlan:[...this.state.confirmPhonePlan, phonePlanFromChild]})
            }
            
        }

    handleChange = input => e =>{
        if(input === 'ICP' || input === 'eICP' || input === 'gasICP' || input === 'license5a' || input === 'passportNo'){
            this.setState({[input]: e.target.value.toUpperCase()})
        }
        else if(input === 'email' || input === 'confirmEmail' || input === 'onlineEmail'){
            this.setState({[input]: e.target.value.toLowerCase()})
        }
        else if(input === 'sameAddress'){
            this.setState({[input]: e.target.checked});
            if(e.target.checked === true){
            this.setState({
                unitOrStNoPost: this.state.unitOrStNo,
                streetAddressPost: this.state.streetAddress,
                suburbPost: this.state.suburb,
                townOrCityPost: this.state.townOrCity,
                postcodePost: this.state.postcode,
            })
            }
        }
        else if(input === 'sameEmail'){
            this.setState({[input]: e.target.checked});
            if(e.target.checked === true){
            this.setState({
                onlineEmail: this.state.email,
            })
            }
        }
        else{
            this.setState({[input]: e.target.value});
        }

    }
    runValidation = (currentStep,steps)=>{
       
        let validationArray = {}
        let dobYear = new Date(this.state.dateOfBirth).getFullYear()
        let authDobYear = new Date(this.state.authorisedDateOfBirth).getFullYear()
        let dobStatus = this.checkIfEighteen(dobYear)
        let authDobStatus = this.checkIfEighteen(authDobYear)
        
         
        switch(currentStep){
            case 0:
                
                if(this.state.firstName === '' || 
                this.state.lastName ===  '' || 
                this.state.email === '' ||  
                this.state.mobilePhone ===  ''|| 
                this.state.confirmEmail === '' 

                ){
                    validationArray = {
                        'status' :  false,
                        'message':'Please ensure all required fields are completed'
                    }
                }
                else if(this.state.title === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please select a title for customer'
                    }
                }
                else if(!dobStatus){
                    validationArray = {
                        'status' :  false,
                        'message':'Customer must be 18 or over to proceed'
                    }
                }else if(this.state.email !==  this.state.confirmEmail){
                    validationArray = {
                        'status' :  false,
                        'message':'Please ensure emails are entered correctly.'
                    }
                }
                else if(this.state.authorisedFirstName !== '' ){
                    
                   if(!authDobStatus){
                        validationArray = {
                            'status' :  false,
                            'message':'Authorised contact must be 18 or over to proceed'
                        }
                    }
                    else{
                        
                        validationArray = {
                            'status' :  true,
                            'message':'all good to proceed'
                        }

                    }
                }
                else if(this.state.identificationType === 'Driver License' ){
                    if(this.state.license5a === '' || 
                    this.state.license5b ===  '' ){
                        validationArray = {
                            'status' :  false,
                            'message':'Please ensure identification details are entered'
                        }
                    }
                    else{
                        validationArray = {
                            'status' :  true,
                            'message':'all good to proceed'
                        }
                    }
                }
                    else if(this.state.identificationType === 'Passport' ){
                        if(this.state.passportNo === '' || 
                        this.state.passportCountry ===  '' ||
                        this.state.passportExpiry ===  '' ){
                            validationArray = {
                                'status' :  false,
                                'message':'Please ensure identification details are entered'
                            }
                        }
                        else{
                            validationArray = {
                                'status' :  true,
                                'message':'all good to proceed'
                            }
                        }
                    
                    
                }
                else{
                        validationArray = {
                            'status' :  true,
                            'message':'all good to proceed'
                        }
                      
                        this.setState({newDateOfBirth: this.state.dateOfBirth.toDateString()})
                       
                        
                    }
                    
                
                
                return validationArray;
            case 1:
                if(this.state.unitOrStNo === '' ||
                this.state.streetAddress === '' ||
                this.state.townOrCity === '' ||
                this.state.postcode === '' ||
                this.state.ICP === ''
                ){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter all required fields'
                    }
                }
                else if(this.state.ICP.length < 15){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the correct ICP'
                    }
                }
                else{
                    validationArray = {
                        'status' :  true
                    }
                }
             
               
                return validationArray;
            case 2:
                if(this.state.unitOrStNoPost === '' ||
                this.state.streetAddressPost === '' ||
                this.state.townOrCityPost === '' ||
                this.state.postcodePost === '' ){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter all required fields'
                    }
                }else{
                    validationArray = {
                        'status' :  true,
                        'message':'all good to proceed'
                    }
                }
               
                return validationArray;
            case 3:
                if(!this.state.selectedServices.electricity  && !this.state.selectedServices.gas && !this.state.selectedServices.lpg){
                    validationArray = {
                        'status' :  false,
                        'message':'Please select at least one service'
                    }
                }
                else if(this.state.ICP && this.state.ICP.length < 15){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the correct ICP'
                    }
                }
                else{
                    // console.log(this.state.ICP.length)
                        validationArray = {
                            'status' :  true,
                            'message':'all good to proceed'
                        }
                    
                        
                }
                  return validationArray;
            case 4:
                let broadbandDetailsObj = this.state.broadbandDetails[0];
                let selectedbroadbandPlan = this.state.broadbandPlan[0];
                
                if(this.state.broadbandPlans.length > 0 && this.state.selectedServices.broadband && selectedbroadbandPlan.plan === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please confirm a broadband plan'
                    }
                } 
                else if( this.state.selectedServices.broadband && selectedbroadbandPlan.plan === '' && this.state.desiredPlanDetails === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter plan details'
                    }
                }
                else if(broadbandDetailsObj.haveISP === 'Yes' && (broadbandDetailsObj.currentBroadbandProvider === '' || broadbandDetailsObj.AccountNumber === '')){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter your current broadband provider details'
                    }
                }else if(broadbandDetailsObj.haveISP === 'Yes' && !broadbandDetailsObj.sameAccountName && broadbandDetailsObj.AccountName === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter your current broadband provider account name'
                    }
                }
                else if((broadbandDetailsObj.haveISP === 'No' && broadbandDetailsObj.haveFibre === 'No' && selectedbroadbandPlan.plan !== 'Regular') && (broadbandDetailsObj.scopeDate === undefined || broadbandDetailsObj.installationDate === undefined)){
                   
                    validationArray = {
                        'status' :  false,
                        'message':'Please select a date for scope and installation date'
                    }
                }
                else if((broadbandDetailsObj.haveISP === 'No' && broadbandDetailsObj.haveFibre === 'Yes' && selectedbroadbandPlan.plan !== 'Regular') && broadbandDetailsObj.connectionDate === undefined){
                   
                    validationArray = {
                        'status' :  false,
                        'message':'Please choose a connection date '
                    }
                }
                else if((selectedbroadbandPlan.plan !== 'Regular' && broadbandDetailsObj.haveISP === 'Yes'&& broadbandDetailsObj.haveFibre === 'Yes') && broadbandDetailsObj.connectionDate === undefined){
                    validationArray = {
                        'status' :  false,
                        'message':'Please choose a connection date'
                    }
                }
                else if((selectedbroadbandPlan.plan === 'Regular' && broadbandDetailsObj.haveISP === 'Yes' ) && broadbandDetailsObj.connectionDate === undefined){
                    validationArray = {
                        'status' :  false,
                        'message':'Please choose a connection date'
                    }
                }
                else if((selectedbroadbandPlan.plan !== 'Regular' && broadbandDetailsObj.haveISP === 'Yes' && broadbandDetailsObj.haveFibre === 'No') && (broadbandDetailsObj.scopeDate === undefined || broadbandDetailsObj.installationDate === undefined)){
                    validationArray = {
                        'status' :  false,
                        'message':'Please select a date for scope and installation date'
                    }
                }else if( broadbandDetailsObj.getLandline === 'Yes' && broadbandDetailsObj.haveLandline === 'Yes' && 
                broadbandDetailsObj.LandlineNumber === '' ){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter your existing landline number'
                    }
                }
                else if(broadbandDetailsObj.getLandline === 'Yes' && broadbandDetailsObj.haveLandline === 'Yes' && broadbandDetailsObj.sameLandlineISP === 'No' && (
                    broadbandDetailsObj.currentLandlineProvider === '' || broadbandDetailsObj.landlineAccountNumber === '' || broadbandDetailsObj.landlineAccountName === '')){
                        validationArray = {
                            'status' :  false,
                            'message':'Please enter your current landline provider details'
                        }
                    }
                else{
                    
                    validationArray = {
                        'status' :  true,
                        'message':'all good to proceed'
                    }
                
                }
                  return validationArray;
            case 5:
                if(this.state.medicallyDependant === '' || this.state.otherMedicallyDependant === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please confirm the following'
                    }
                }else{
                    validationArray = {
                        'status' :  true,
                        'message':'all good to proceed'
                    }
                
                }
                return validationArray;
            case 6:
                
                if(this.state.selectedServices.electricity && Object.keys(this.state.electricityRates).length === 0){
                    
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the rates by tapping on the pencil icon.'
                    }
                }else if((this.state.selectedServices.lpg || this.state.selectedServices.gas) && Object.keys(this.state.gasRates).length === 0){

                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the gas rates'
                    }
                }
                else if(this.state.electricityRates[0].rateWithPulse === 0){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the Fixed (Daily) rates with Pulse'
                    }   
                }
                else if(this.state.electricityRates[1].rateWithPulse === 0){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the Variable meter 1 rates with Pulse'
                    }   
                }
                else if(this.state.selectedServices.gas && this.state.gasRates[0].naturalGas === 0){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the natural gas rates'
                    }   
                }
                else if(this.state.selectedServices.lpg && this.state.gasRates[0].lpgRate === 0){
                    validationArray = {
                        'status' :  false,
                        'message':'Please enter the lpg rates'
                    }   
                }
                else{
                    validationArray = {
                        'status' :  true,
                        'message':'all good to proceed'
                    }
                
                }
                return validationArray;
            case 7:
                if(this.state.paymentMethod === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please select your preferred payment method'
                    }
                }else{
                    validationArray = {
                        'status' :  true,
                        'message':'all good to proceed'
                    }
                
                }
                return validationArray;
            case 8:
                validationArray = {
                    'status' :  true,
                    'message':'all good to proceed'
                }
                return validationArray;
            case 9:
                if(this.state.signatureURL === ''){
                    validationArray = {
                        'status' :  false,
                        'message':'Please save your signature'
                    }
                } 
                else if(this.state.company === ''){

                    validationArray = {
                        'status' :  false,
                        'message':'Please select your company name'
                    }
                }
                else if(this.state.representativeName === ''){

                    validationArray = {
                        'status' :  false,
                        'message':'Please enter representative name'
                    }
                }
                else{
                        validationArray = {
                            'status' :  true,
                            'message':'all good to proceed'
                        }
                    }
                return validationArray;
            
        }
        
    }
    componentDidMount=()=>{
      
        this.initialDataFromBackend();
    }
    componentDidUpdate(_prevProps, prevState,snapshot) {
    
        if(this.state.dateOfMoveIn !== null && prevState.dateOfMoveIn !== this.state.dateOfMoveIn){
            this.setState({newDateOfMoveIn: this.state.dateOfMoveIn.toDateString()})
        }
      
        if(this.state.dateOfBirth !== null && prevState.dateOfBirth !== this.state.dateOfBirth){

            this.setState({newDateOfBirth: this.state.dateOfBirth.toDateString()})
        }
        if(this.state.passportExpiry !== null && prevState.passportExpiry !== this.state.passportExpiry){

            this.setState({newPassportExpiry: this.state.passportExpiry.toDateString()})
        }
        if(this.state.authorisedDateOfBirth !== null && prevState.authorisedDateOfBirth !== this.state.authorisedDateOfBirth){

            this.setState({newAuthorisedDateOfBirth: this.state.authorisedDateOfBirth.toDateString()})
        }
        if(!this.state.selectedServices.broadband && prevState.selectedServices.broadband !== this.state.selectedServices.broadband){

            this.setState({broadbandPlan: this.initialBBState()})

        }
      }

    initialBBState=()=>{
        return [{
            plan:'',
            connection:'',
            price:''
        }
        ]
    }
    getDOB =(dateFromChild)=>{
        this.setState({dateOfBirth: dateFromChild })
        
        
    }
    checkIfEighteen =(yearOfBirth)=>{
        const age = this.state.currentYear - yearOfBirth
        if(age <= 18){
            return false;
        }else{
            return true;
        }
        
    }
    getAuthDOB=(dateFromChild)=>{
        this.setState({authorisedDateOfBirth: dateFromChild})
    }
    getMoveInDate=(dateFromChild)=>{
        this.setState({dateOfMoveIn: dateFromChild})
    }
    getPassportExpiry=(dateFromChild)=>{
        this.setState({passportExpiry: dateFromChild})
    }

    setTitle = (titleFromChild)=>{
        this.setState({title: titleFromChild})
    }

    setSignature = (signatureFromChild) =>{
        this.setState({signatureURL:signatureFromChild});
    }

    setElectricityMeterLocation = (locationFromChild)=>{
        this.setState({eMeterLocation:locationFromChild});
    }
    setGasMeterLocation = (locationFromChild)=>{
        this.setState({gasMeterLocation:locationFromChild});
    }
    setSelectedServices = (servicesFromChild)=>{
        
        this.setState({selectedServices:servicesFromChild})
        
        if(servicesFromChild.lpg){
            this.setState({lpgService:"Yes, sign me up now"})
        }else{
            this.setState({lpgService:""})
        }

       
        
    }
    setElectricityRates =(electricityRatesFromChild)=>{
        this.setState({electricityRates:electricityRatesFromChild})
    }
    setGasRates=(gasRatesFromChild)=>{
        this.setState({gasRates:gasRatesFromChild});
       
    }
    setBBPlan = (selectedBroadbandPlan) => {
        this.setState({broadbandPlan:selectedBroadbandPlan})
    }
    
    isLoading=()=>{
        this.setState({loading: true})
    }

    setAllBroadbandPlans=(allBroadbandPlansFromChild)=>{
        this.setState({broadbandPlans:allBroadbandPlansFromChild})
    }

    getAllBroadbandPlans=()=>{
        
       let address = this.state.unitOrStNo +' '+ this.state.streetAddress + ', ' + this.state.townOrCity
        // let address = '21 lagoon drive, panmure'
       
        let data = {address}
        let success = false;
        this.setState({loadingBroadband: true})
        this.setState({broadbandPlan:[{
            plan:'',
            connection:'',
            price:''
        }
        ]})
        axios.post(window.location.href+`/getBroadbandPlans`, data)
        .then(res => {
           
            const result = res.data
            success = result.success
            //set bb result
            if(success){
                this.setState({broadbandPlans:result.data})
            }else{
                this.setState({broadbandPlans:""})
            }

            this.setState({loadingBroadband: false})
      }).catch(function (error) {  
        alert("Oops, the api is currently not available. Please re-enter address in step 2 and try again")
    
      })
    }
    initialDataFromBackend=()=>{
    axios.get(window.location.href+'/getDataFromBackend/')
    .then(response=> {
      // handle success
      const result = response.data;
      let terms = result.terms
      //setState for terms
      this.setState({terms: terms});
      this.setState({broadbandTerms: result.broadbandTerms})
      this.setState({
          phonePlans:result.phonePlans,
      },()=>{
          this.setState({
              confirmPhonePlan:[{
                  title:this.state.phonePlans[0].title,
                  details:this.state.phonePlans[0].details,
                  price:this.state.phonePlans[0].price
              }]
          })
      })
      
      
    })
    .catch(error=> {
      // handle error
      console.log(error);
    })
  }

    handleSubmission = (lastStep,steps) =>{
        
        const {title,firstName,middleName,lastName,accountName,dateOfBirth,newDateOfBirth,mobilePhone,homePhone,email,confirmEmail,
            isAccountHolder,license5a,license5b,onlineOrPost,onlineEmail,authorisedContactTitle,authorisedFirstName,
            authorisedLastName,authorisedDateOfBirth,billedByAnotherCompany,dateOfMoveIn,unitOrStNo,streetAddress,suburb,townOrCity,postcode,ICP,
            unitOrStNoPost,streetAddressPost,suburbPost,townOrCityPost,postcodePost,accessNote,medicallyDependant,otherMedicallyDependant,paymentMethod,signatureURL,
        gasICP,eMeterNotes,gasMeterNotes,selectedServices,eMeterLocation,gasMeterLocation,electricityRates,gasRates,representativeName,
        electricityUserType,gasUserType,noticeToCustomer,lpgService,identificationType,passportNo,passportCountry,passportExpiry,customerSituation,sameAddress,sameEmail,comments,
        company,interestInSolar,newAuthorisedDateOfBirth,newDateOfMoveIn,newPassportExpiry,broadbandPlan,desiredPlanDetails,broadbandDetails,confirmPhonePlan}=this.state;
        const application_data = {title,firstName,middleName,lastName,accountName,dateOfBirth,newDateOfBirth,mobilePhone,homePhone,email,confirmEmail,
            isAccountHolder,license5a,license5b,onlineEmail,onlineOrPost,authorisedContactTitle,authorisedFirstName,
            authorisedLastName,authorisedDateOfBirth,billedByAnotherCompany,dateOfMoveIn,unitOrStNo,streetAddress,suburb,townOrCity,postcode,ICP,
            unitOrStNoPost,streetAddressPost,suburbPost,townOrCityPost,postcodePost,accessNote,medicallyDependant,otherMedicallyDependant,paymentMethod,signatureURL,
        gasICP,eMeterNotes,gasMeterNotes,selectedServices,eMeterLocation,gasMeterLocation,electricityRates,gasRates,representativeName,
        electricityUserType,gasUserType,noticeToCustomer,lpgService,identificationType,passportNo,passportCountry,passportExpiry,customerSituation,sameAddress,sameEmail,comments,
        company,interestInSolar,newAuthorisedDateOfBirth,newDateOfMoveIn,newPassportExpiry,broadbandPlan,desiredPlanDetails,broadbandDetails,confirmPhonePlan};
        
        
        let applicationData = { application_data }
        // console.log(applicationData.dateOfBirth)
        const divElement = document.getElementById('root').lastChild;
        const divHTML = document.createElement("div");
        const submitFormButton = document.getElementById('submitFormButton');
        this.setState({loading: true})
      
        axios.post(window.location.href+`/DtoDsignupFormHandler`,  applicationData)
      .then(res => {
          
         divHTML.innerHTML = "<p>Thanks for submitting the form, a copy of the form details has been sent to the customer and our team. Click below to start a new application.</p><br/><button class='button' onClick='window.location.reload();'>New application</a>";

        const status = res.data;
        console.log(status);
        if(status){
           if(submitFormButton!= undefined){
            submitFormButton.style.display = 'none';
           }
            divElement.appendChild(divHTML);
             this.setState({loading: false})
        }else{
            //alert to contact developer
            alert("Oops, something went wrong. Please contact Pulse Energy Developer for more information.")
            this.setState({loading: false})
        }
      }).catch(function (error) {
        console.log("Oops, something went wrong. Please contact Pulse Energy Developer for more information.")

      })
        
}
    render() {
        
        const {loading,title,firstName,middleName,lastName,accountName,dateOfBirth,mobilePhone,homePhone,email,confirmEmail,
            isAccountHolder,license5a,license5b,onlineOrPost,onlineEmail,authorisedContactTitle,authorisedFirstName,
            authorisedLastName,authorisedDateOfBirth,billedByAnotherCompany,dateOfMoveIn,unitOrStNo,streetAddress,suburb,townOrCity,postcode,ICP,
            unitOrStNoPost,streetAddressPost,suburbPost,townOrCityPost,postcodePost,accessNote,medicallyDependant,otherMedicallyDependant,paymentMethod,signatureURL,
        gasICP,interestInBroadband,eMeterNotes,gasMeterNotes,selectedServices,lpgService,eMeterLocation,gasMeterLocation,electricityRates,gasRates,representativeName,electricityUserType,gasUserType,
        identificationType,passportNo,passportCountry,passportExpiry,customerSituation,sameAddress,sameEmail,comments,company,interestInSolar,broadbandPlan,broadbandPlans,loadingBroadband,desiredPlanDetails,confirmPhonePlan,
        terms,broadbandTerms,phonePlans,broadbandDetails}=this.state;
        const values = {loading,title,firstName,middleName,lastName,accountName,dateOfBirth,mobilePhone,homePhone,email,confirmEmail,
            isAccountHolder,license5a,license5b,onlineEmail,onlineOrPost,authorisedContactTitle,authorisedFirstName,
            authorisedLastName,authorisedDateOfBirth,billedByAnotherCompany,dateOfMoveIn,unitOrStNo,streetAddress,suburb,townOrCity,postcode,ICP,
            unitOrStNoPost,streetAddressPost,suburbPost,townOrCityPost,postcodePost,accessNote,medicallyDependant,otherMedicallyDependant,paymentMethod,signatureURL,
        gasICP,interestInBroadband,eMeterNotes,gasMeterNotes,selectedServices,lpgService,eMeterLocation,gasMeterLocation,electricityRates,gasRates,representativeName,electricityUserType,gasUserType,
        identificationType,passportNo,passportCountry,passportExpiry,customerSituation,sameAddress,sameEmail,comments,company,interestInSolar,broadbandPlan,broadbandPlans,loadingBroadband,desiredPlanDetails,confirmPhonePlan,terms
        ,broadbandTerms,phonePlans,broadbandDetails};
        return (
            <div>
                <MasterForm 
                values={values} 
                handleChange={this.handleChange} 
                getDOB={this.getDOB} 
                getAuthDOB={this.getAuthDOB} 
                getMoveInDate={this.getMoveInDate} 
                setSignature={this.setSignature}
                handleSubmission={this.handleSubmission} 
                setBBPlan={this.setBBPlan}
                setAllBroadbandPlans={this.setAllBroadbandPlans}
                setSelectedServices={this.setSelectedServices}
                setGasMeterLocation={this.setGasMeterLocation}
                setElectricityMeterLocation={this.setElectricityMeterLocation}
                setElectricityRates={this.setElectricityRates}
                setGasRates={this.setGasRates}
                runValidation={this.runValidation}
                getAllBroadbandPlans={this.getAllBroadbandPlans}
                getPassportExpiry={this.getPassportExpiry}
                setBroadbandInfo={this.setBroadbandInfo}
                setPhonePlan={this.setPhonePlan}
                />
                { this.state.loading && <CircularProgress />}
            </div>
        )
    }
}
