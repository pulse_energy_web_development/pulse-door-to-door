import React from 'react';
import './App.css';
import HomePage from './components/HomePage';
import 'bootstrap/dist/css/bootstrap.css';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

function App() {
  return (
    <div className="SignUpApp container">
    
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <HomePage />
          </MuiPickersUtilsProvider>
      
    </div>
  );
}

export default App;
