import React from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CardActionArea from '@material-ui/core/CardActionArea';
import Radio from '@material-ui/core/Radio';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import 'react-confirm-alert/src/react-confirm-alert.css'; 
const useStyles = makeStyles({
    root: {
      maxWidth: "100%",
      textAlign:'left',
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12
    },
    content:{
      minHeight:'150px!important',
      borderTop: "3px solid #59a946"
    },
    selectedContent:{
      minHeight:'150px!important',
      borderTop: "3px solid #00A9E2"
    },
    headingSection:{
      display: "flex",
      justifyContent:"space-between",
      flexDirection:"row",
      alignItems: "baseline"
    },
    action:{
      width: '100px',
      justifyContent: "flex-end",
      color:'grey',
      float:'right'
    },
    iconButton:{
      padding: "0!important"
    },
    
    checkCircle:{
      color: "#EBEFEE",
    },
    accordion:{
      border: 'none',
      boxShadow: 'none'
    },
    accordionSummary:{
      width: "auto",
    display: "flex",
    justifyContent: "flex-end",
    flexDirection: "row",
    alignItems: "center"
    },
    expandIcon:{
      padding:'0!important'
    },
    icon: {
      borderRadius: '50%',
      width: 22,
      height: 22,
      boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
      backgroundColor: '#f5f8fa',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
      '$root.Mui-focusVisible &': {
        outline: '2px auto rgba(19,124,189,.6)',
        outlineOffset: 2,
      },
      'input:hover ~ &': {
        backgroundColor: '#00a9e2',
      },
      'input:disabled ~ &': {
        boxShadow: 'none',
        background: 'rgba(206,217,224,.5)',
      },
    },
    checkedIcon: {
      backgroundColor: 'transparent',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
      '&:before': {
        display: 'block',
        width: 22,
        height: 22,
        backgroundImage: "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggc3R5bGU9ImZpbGw6IzFFOTBGRjsiIGQ9Ik0yMjUuODgyLDMzMS4yOTRjLTcuOTgxLTAuMDMtMTUuNjE2LTMuMjIzLTIxLjIzMy04Ljg4NWwtNDUuMTc2LTQ1LjE3Ng0KCWMtMTEuNzYxLTExLjc2MS0xMS43NjEtMzAuODU2LDAtNDIuNjE2czMwLjg1Ni0xMS43NjEsNDIuNjE2LDBsMjUuNDQ5LDI1LjQ0OWw4NC4xNzktNzIuMTMyDQoJYzEzLjgwOS05LjI3NiwzMi41MjctNS41ODcsNDEuNzg4LDguMjIyYzcuNjk1LDExLjQ3NSw2LjYxMSwyNi42OTktMi42MzUsMzYuOTU0bC0xMDUuNDEyLDkwLjM1Mw0KCUMyNDAuMDgzLDMyOC4zNDMsMjMzLjE0MSwzMzEuMTI4LDIyNS44ODIsMzMxLjI5NHoiLz4NCjxwYXRoIHN0eWxlPSJmaWxsOiMxNjZDQkY7IiBkPSJNMjU2LDUxMkMxMTQuNjEzLDUxMiwwLDM5Ny4zODcsMCwyNTZTMTE0LjYxMywwLDI1NiwwczI1NiwxMTQuNjEzLDI1NiwyNTZTMzk3LjM4Nyw1MTIsMjU2LDUxMnoNCgkgTTI1Niw2MC4yMzVDMTQ3Ljg3OCw2MC4yMzUsNjAuMjM1LDE0Ny44NzgsNjAuMjM1LDI1NlMxNDcuODc4LDQ1MS43NjUsMjU2LDQ1MS43NjVTNDUxLjc2NSwzNjQuMTIyLDQ1MS43NjUsMjU2DQoJUzM2NC4xMjIsNjAuMjM1LDI1Niw2MC4yMzV6Ii8+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg==)",
        content: '""',
      },
      'input:hover ~ &': {
        backgroundColor: '#00a9e2',
      },
    },
    vdslFontColor:{
      color:"#59A946"
    },
    fibreFontColor:{
      color:"#00A9E2"
    }
  });

export default function BroadbandSummaryRow(props) {
    const classes = useStyles();
    const [show, toggleShow]=React.useState(false)
    const [currentPlan, setCurrentPlan]= React.useState([{'plan':'',
    'connection':'',
    'price': ''}])
    const [selected, setSelected] = React.useState(false)
    
    let planArray = [];

    const toggleShowOrHide = ()=>{
      if(show){
        toggleShow(!show)
      }else{
        toggleShow(true)
      }
    }
   
    const handleOptionChange=(e,plan,connection,price)=>{
     
      planArray.push(
        {'plan':plan,
        'connection':connection,
        'price': price})
      props.setBBPlan(planArray)
      
     
    }

    React.useEffect(()=>{
     
    },[selected,currentPlan])

    return (
  // 0000120411TR376
      <div className="col-xs-12 col-md-6 col-lg-6 d-flex justify-content-center align-items-start broadband" key={props.index}>
        <Card className={classes.root} id={classes.isChecked}>
          
        <CardActionArea onClick={(e)=>handleOptionChange(e,props.obj.Title,props.obj.Connection,props.obj.Price)}>
        <CardContent className={props.defaultPlan === props.obj.Title? 
                  classes.selectedContent :
                  classes.content}>
        
       
        {/* </CardActions> */}
          <Typography variant="h5" component="h2" className={classes.headingSection}>
          {props.obj.Title} 
      {props.defaultPlan === props.obj.Title  ?  <Radio id="card-input-element" 
       checked={props.defaultPlan === props.obj.Title} value={props.obj.Title} checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}/> 
       : <span style={{padding:"9px 0"}}><RadioButtonUncheckedIcon/></span> }
       {/* <Radio id="card-input-element" 
       checked={props.defaultPlan === props.obj.Title} value={props.obj.Title} checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}/> */}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
          ({props.obj.Connection})
          </Typography>

          <Accordion className={classes.accordion} key={props.obj.Key}>
        <AccordionSummary
          className={classes.accordionSummary}
          expandIcon={<ExpandMoreIcon className={classes.expandIcon}/>}
          aria-controls="panel2-content"
          id="panel2-header"
          onClick={()=>toggleShowOrHide()}
        ><Typography style={{fontSize:"1.2rem!important"}}>{show ? 'Hide' : 'Show more'}</Typography>
        </AccordionSummary>
        <AccordionDetails>
        <Typography variant="h5" component="h5" >
          {props.obj.Details}
            <br />
           
          </Typography>
        </AccordionDetails>
      </Accordion>
      <hr/>
            <h4>BundleUp Rate</h4>
            {/* {props.obj.Title === 'Regular' ?  <Typography className={classes.vdslFontColor}> <span style={{fontSize:'4rem'}}>{props.obj.Price}</span> <b>/month +GST</b></Typography>: <Typography className={classes.fibreFontColor}> <span style={{fontSize:'4rem'}}>{props.obj.Price}</span> <b>/month +GST</b></Typography>} */}
            <Typography> <span style={{fontSize:'4rem'}}>{props.obj.Price}</span> <b>/month +GST</b></Typography>
            <Typography>Standard Rate {props.obj.StandardRate}/month +GST</Typography> 
          
        </CardContent>
        </CardActionArea>
     </Card>
     </div>

    )
}
