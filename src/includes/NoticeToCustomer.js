import React from 'react'

export default function NoticeToCustomer() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <h5>You can cancel or withdraw from this Agreement within 5 working days of signing this Agreement, by contacting us.</h5>
                </div>
            </div>
            
        </div>
    )
}
