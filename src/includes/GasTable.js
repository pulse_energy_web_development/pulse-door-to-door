import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Input from "@material-ui/core/Input";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
// Icons
import EditIcon from "@material-ui/icons/EditOutlined";
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import RevertIcon from "@material-ui/icons/NotInterestedOutlined";
const useStyles = makeStyles(theme => ({
    root: {
      width: "100%",
      marginTop: theme.spacing(3),
      overflowX: "auto"
    },
    table: {
      minWidth: 650
    },
    selectTableCell: {
      width: 60
    },
    tableCell: {
      width: 130,
      height: 40
    },
    input: {
      width: 130,
      height: 40
    }
  }));

  const createGasData = (name, naturalGas, lpgRate) => ({
    id: name.replace(" ", "_"),
    name,
    naturalGas,
    lpgRate,
    isEditMode: false
  });


  const CustomTableCell = ({ row, name, onChange }) => {
    const classes = useStyles();
    const { isEditMode } = row;
    return (
      <TableCell align="left" className={classes.tableCell}>
          {isNaN(parseInt(row[name])) && row[name] !== '' ? row[name] : (<Input
            value={row[name]}
            name={name}
            type='number'
            onChange={e => onChange(e, row)}
            className={classes.input}
          />)}
        {/* {isEditMode ? (
          <Input
            value={row[name]}
            name={name}
            onChange={e => onChange(e, row)}
            className={classes.input}
          />
        ) : (
          row[name]
        )} */}
      </TableCell>
    );
  };



export default function GasTable(props) {
    const [rows, setRows] = React.useState(props.gasRates);
      const [previous, setPrevious] = React.useState({});
      const classes = useStyles();
      const onToggleEditMode = id => {
        setRows(state => {
          return rows.map(row => {
            if (row.id === id) {
              return { ...row, isEditMode: !row.isEditMode };
            }
            return row;
          });
        });
      };
    
      const onChange = (e, row) => {
        if (!previous[row.id]) {
          setPrevious(state => ({ ...state, [row.id]: row }));
        }
        const value = e.target.value;
        const name = e.target.name;
        const { id } = row;
        const newRows = rows.map(row => {
          if (row.id === id) {
            return { ...row, [name]: value };
          }
          return row;
        });
        setRows(newRows);
        props.getGasRates(newRows)
      };
    
      const onRevert = id => {
        const newRows = rows.map(row => {
          if (row.id === id) {
            return previous[id] ? previous[id] : row;
          }
          return row;
        });
        setRows(newRows);
        setPrevious(state => {
          delete state[id];
          return state;
        });
        onToggleEditMode(id);
      };
    return (
        <Paper className={classes.root}>
        <Table className={classes.table} aria-label="caption table" id="gasRatesTable">
          <TableHead>
          
            <TableRow>
            <TableCell align="left" />
              <TableCell align="left" />
              <TableCell align="left">Natural Gas (cents)</TableCell>
              <TableCell align="left">LPG ($)</TableCell>
        
            </TableRow>
          </TableHead>
          <TableBody>
     
            {rows.map(row => (
              <TableRow key={row.id}>
                
                <TableCell className={classes.selectTableCell}>
                  {/* {row.isEditMode ? (
                    <>
                      <IconButton
                        aria-label="done"
                        onClick={() => onToggleEditMode(row.id)}
                      >
                        <DoneIcon />
                      </IconButton>
                      <IconButton
                        aria-label="revert"
                        onClick={() => onRevert(row.id)}
                      >
                        <RevertIcon />
                      </IconButton>
                    </>
                  ) : (
                    <IconButton
                      aria-label="delete"
                      onClick={() => onToggleEditMode(row.id)}
                    >
                      <EditIcon />
                    </IconButton>
                  )} */}
                </TableCell>
                    
                <CustomTableCell style={{fontSize:"1rem!important"}} {...{ row, name: "name", onChange }} />
                <CustomTableCell {...{ row, name: "naturalGas", onChange }} />
                <CustomTableCell {...{ row, name: "lpgRate", onChange }} />
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    )
}
