import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

export default function MedicallyDependent(props) {
    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                <FormLabel component="legend">Are you dependant on medical or life support equipment that relies on electricity?</FormLabel>
                    <RadioGroup row aria-label="medicallyDependant" label="Online or Post bill" name="medicallyDependant" value={props.values.medicallyDependant} onChange={props.handleChange('medicallyDependant')} defaultValue={props.values.medicallyDependant}>
                        <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                        <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
                    </RadioGroup>
                    <FormLabel component="legend">To prevent a clear threat to your health or well-being, is anyone else in your home reliant on electricity for reason of age, health or disability?</FormLabel>
                    <RadioGroup row aria-label="otherMedicallyDependant" label="Online or Post bill" name="otherMedicallyDependant" value={props.values.otherMedicallyDependant} onChange={props.handleChange('otherMedicallyDependant')} defaultValue={props.values.otherMedicallyDependant}>
                        <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                        <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
                    </RadioGroup>
                    </div>
                </div>
        </div>
    )
}
