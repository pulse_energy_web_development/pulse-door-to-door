import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Input from "@material-ui/core/Input";
import Paper from "@material-ui/core/Paper";
import GasTable from "./GasTable";
const useStyles = makeStyles(theme => ({
    root: {
      width: "100%",
      marginTop: theme.spacing(3),
      padding: '6px',
      fontSize: '1.8rem',
      overflowX: "auto"
    },
    table: {
      minWidth: "100%"
    },
    selectTableCell: {
      width: 60
    },
    tableCell: {
      width: 130,
      height: 40
    },
    input: {
      width: 130,
      height: 40,
      
    }
  }));
  
  const CustomTableCell = ({ row, name, onChange }) => {
    const classes = useStyles();
    const { isEditMode } = row;
    return (   
      <TableCell align="left" className={classes.tableCell}>
        {isNaN(parseInt(row[name])) && (row[name] !== '' ) ? row[name] : (<Input
            value={row[name]}
            name={name}
            type='number'
            onChange={e => onChange(e, row)}
            className={classes.input}
          />)}
      </TableCell>
   
    );
  };


export default function PlanSummary(props) {
    const [rows, setRows] = React.useState(props.values.electricityRates);
      const [previous, setPrevious] = React.useState({});
      const classes = useStyles();
      const onToggleEditMode = id => {
        setRows(state => {
          return rows.map(row => {
            if (row.id === id) {
              return { ...row, isEditMode: !row.isEditMode };
            }
            return row;
          });
        });
      };

      React.useEffect(()=>{
        
        if(rows.length > 0  ){
         
          let lastPos = rows.length - 1;
          let secondToLastPos = rows.length - 2;
          const hypen = "-"
          if(rows[lastPos].currentRate !== hypen){
            
             //Price Promise.CurrentRate disabled
            rows[lastPos].currentRate = hypen;
            //Competitor PPD fields disable
            rows[secondToLastPos].rateWithPulse = hypen;
            rows[secondToLastPos].pricePromiseRate = hypen;
          }
        }
    },[rows])

      const onChange = (e, row) => {
        if (!previous[row.id]) {
          setPrevious(state => ({ ...state, [row.id]: row }));
         
        }
        const value = e.target.value;
        const name = e.target.name;
        const { id } = row;
        const newRows = rows.map(row => {
          if (row.id === id) {
            return { ...row, [name]: value };
          }
          return row;
        });
        setRows(newRows);
        props.getElectricityRates(newRows)
      };
    
      const onRevert = id => {
        const newRows = rows.map(row => {
          if (row.id === id) {
            return previous[id] ? previous[id] : row;
          }
          return row;
        });
        setRows(newRows);
        setPrevious(state => {
          delete state[id];
          return state;
        });
        onToggleEditMode(id);
      };
      const getGasRates = (ratesFromGasTable) =>{
        props.getGasRates(ratesFromGasTable)
      }


     

    return (
        <div >
        <Paper className={classes.root}>
        <Table className={classes.table} aria-label="caption table" id="planSummary">
          <TableHead>
          
            <TableRow>
            <TableCell align="left" />
              <TableCell align="left" >Electricity</TableCell>
              <TableCell align="left">Your current rate (before PPD) (cents)</TableCell>
              <TableCell align="left">Your rate with Pulse Energy (cents)</TableCell>
              <TableCell align="left">Your rate after Price Promise (cents)</TableCell>
        
            </TableRow>
          </TableHead>
          <TableBody>
          
            {rows.map(row => (
              <TableRow key={row.id}>
                
                
                <TableCell className={classes.selectTableCell} >
                </TableCell>
                    
                <CustomTableCell style={{fontSize:"1rem!important"}} {...{ row, name: "name",onChange  }} />
                <CustomTableCell {...{ row, name: "currentRate", onChange }} />
                <CustomTableCell {...{ row, name: "rateWithPulse", onChange }} />
                <CustomTableCell {...{ row, name: "pricePromiseRate", onChange }} />
              </TableRow>
            ))}
          </TableBody>
        </Table>
    
      </Paper>
        {(props.values.selectedServices.gas || props.values.selectedServices.lpg) && <GasTable getGasRates={getGasRates} gasRates={props.values.gasRates}/> } 
        <br/>
        <p style={{fontSize:"1.8rem!important"}}>Please note all rates above are exclusive of GST. Rates above are inclusive of EA Levy.</p>
        <p style={{fontSize:"1.8rem!important"}}>If your meter configuration is different to what we have quoted you on, our standard rates for your meter configuration will apply. If this is the case, we will let you know.</p>
    </div>
    )
}
