import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import DateFnsUtils from '@date-io/date-fns';
import Grid from '@material-ui/core/Grid';
import validator from 'validator';
import Checkbox from '@material-ui/core/Checkbox';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker 
} from '@material-ui/pickers';
import TextField from "@material-ui/core/TextField";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import LandlinePackage from './LandlinePackageSummary';

export default function BroadbandQuestions(props) {
    const startDate  = new Date(new Date().setDate(new Date().getDate()+13));
    const [confirmPhonePlan, setPhonePlan] = React.useState(props.values.confirmPhonePlan);
    const [phonePlanName, setphonePlanName] = React.useState(props.values.confirmPhonePlan[0].title);
    const [phonePlans, getBroadbandPlans] = React.useState(props.values.phonePlans); 

    const [broadbandInfo, setBroadbandInfo] = React.useState({
        AccountNumber: props.values.broadbandDetails[0].AccountNumber,
        AccountName: props.values.broadbandDetails[0].AccountName,
        currentBroadbandProvider: props.values.broadbandDetails[0].currentBroadbandProvider,
        LandlineNumber: props.values.broadbandDetails[0].LandlineNumber,
        currentLandlineProvider: props.values.broadbandDetails[0].currentLandlineProvider,
        landlineAccountNumber: props.values.broadbandDetails[0].landlineAccountNumber,
        landlineAccountName: props.values.broadbandDetails[0].landlineAccountName,
        haveISP: props.values.broadbandDetails[0].haveISP,
        sameAccountName: props.values.broadbandDetails[0].sameAccountName,
        haveJackpoints: props.values.broadbandDetails[0].haveJackpoints,
        haveFibre: props.values.broadbandDetails[0].haveFibre,
        getLandline: props.values.broadbandDetails[0].getLandline,
        haveLandline: props.values.broadbandDetails[0].haveLandline,
        sameLandlineISP: props.values.broadbandDetails[0].sameLandlineISP
    });
    
    const disableWeekends=(date)=> {
        return date.getDay() === 0 || date.getDay() === 6 || date.getDay() === 5;
    }

    const setMinDate=(startDate)=> {
        let today = new Date(new Date().setDate(new Date().getDate()));
        let minDate = startDate;

        if(today.getDay() === 1 ){
            return new Date(new Date().setDate(new Date().getDate()+10));
        }
        if(today.getDay() === 5 ){
            return new Date(new Date().setDate(new Date().getDate()+12));
        }
        return minDate;

    }

    const handleChange=(evt)=> {
      
        const value = evt.target.value;
        if(evt.target.name === 'sameAccountName' || evt.target.name === 'haveJackpoints' ){
            setBroadbandInfo({
                ...broadbandInfo,
                [evt.target.name]: evt.target.checked
              });
        }
        else{
            setBroadbandInfo({
                ...broadbandInfo,
                [evt.target.name]: value
              });
        }
        
      }
      const handleDates=(name,date)=> {

        setBroadbandInfo({
            ...broadbandInfo,
            [name]: date.toDateString()
          });

      }

    const setLandlinePlan=(phonePlan)=>{
        props.setPhonePlan(phonePlan)
        setPhonePlan(phonePlan) 
    }
    const handlePlan = (newPlan) => {
        setphonePlanName(newPlan)
    };
    const displayPhonePlans=()=>{
        let confirmPhonePlan = props.values.confirmPhonePlan;

        return phonePlans.map( function(object, i){
           return <LandlinePackage obj={object} key={i} index={i} confirmPhonePlan={confirmPhonePlan} defaultPlanTitle={phonePlanName} handlePlan={handlePlan} setPhonePlan={setLandlinePlan}></LandlinePackage>
            
        })
    }

    React.useEffect(()=>{
        if(broadbandInfo.sameAccountName){
            broadbandInfo.AccountName = props.values.accountName
        }
        setBroadbandInfo(broadbandInfo)
        props.handleBroadbandDetails(broadbandInfo)
        setPhonePlan(props.values.confirmPhonePlan) 
       
    },[broadbandInfo,broadbandInfo.sameAccountName])

    const setScopeDates = ()=>{
        return (
            <React.Fragment>
                <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <Grid container justify="flex-start" alignItems="baseline">
                    <KeyboardDatePicker
                    style={{marginBottom:"1rem"}}
                    required
                    margin="normal"
                    id="date-picker-dialog"
                    label="Scope Date for your new broadband connection"
                    format="dd/MM/yyyy"
                    value={broadbandInfo.scopeDate}
                    name="scopeDate"
                    allowKeyboardControl={false}
                    minDate={setMinDate(startDate)}
                    shouldDisableDate={disableWeekends}
                    onChange={(date) => handleDates("scopeDate",date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                        </Grid>
                </MuiPickersUtilsProvider>
                <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <Grid container justify="flex-start" alignItems="baseline">
                    <KeyboardDatePicker
                    style={{marginBottom:"1rem"}}
                    required
                    margin="normal"
                    id="date-picker-dialog"
                    label="Install Date for your new broadband connection"
                    format="dd/MM/yyyy"
                    value={broadbandInfo.installationDate}
                    name="scopeDate"
                    allowKeyboardControl={false}
                    minDate={setMinDate(startDate)}
                    shouldDisableDate={disableWeekends}
                    onChange={(date) => handleDates("installationDate",date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                        </Grid>
                </MuiPickersUtilsProvider>
            </React.Fragment>
        )
    }

    const setConnectionDate = ()=>{
       return ( <MuiPickersUtilsProvider utils={DateFnsUtils} >
        <Grid container justify="flex-start" alignItems="baseline">
            <KeyboardDatePicker
            style={{marginBottom:"1rem"}}
            required
            margin="normal"
            id="date-picker-dialog"
            label="Connection Date you would like your broadband to switch to Pulse"
            format="dd/MM/yyyy"
            value={broadbandInfo.connectionDate}
            name="scopeDate"
            allowKeyboardControl={false}
            minDate={setMinDate(startDate)}
            shouldDisableDate={disableWeekends}
            onChange={(date) => handleDates("connectionDate",date)}
            KeyboardButtonProps={{
                'aria-label': 'change date',
            }}
            />
                </Grid>
        </MuiPickersUtilsProvider>
        )
    }

    return ( 
        <div className="row flex-column w-100">
            <legend>Broadband Provider Details</legend>
            <FormLabel component="legend" style={{marginTop:"1rem"}}>Do you currently have ISP?</FormLabel>
            <RadioGroup row aria-label="haveISP" label="Do you currently have ISP?" name="haveISP" value={broadbandInfo.haveISP} onChange={handleChange}>
                <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
            </RadioGroup>
       
        {/* IF YES  */}
        { broadbandInfo.haveISP === 'Yes' &&  
       <React.Fragment>
         <TextField required 
            className="mt-0 w-50"
            name="currentBroadbandProvider" 
            label="Current Broadband Provider" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.currentBroadbandProvider} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.currentBroadbandProvider} 
            helperText={!broadbandInfo.currentBroadbandProvider ? 'This is required' : ' '}/>

        {/* if account name is same as above */}
        <FormControlLabel control={<Checkbox checked={broadbandInfo.sameAccountName} onChange={handleChange} name="sameAccountName" />}
            label="Account name is same as above" />
        
        {/* if the account name is DIFFERENT than the details above then hide */}
        {!broadbandInfo.sameAccountName && 
        <TextField required 
            className="mt-0 w-50"
            name="AccountName" 
            label="Account Name (Current ISP)" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.AccountName} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.AccountName} 
            helperText={!broadbandInfo.AccountName ? 'This is required' : ' '} />}
         
             
             {/* GET Account Number */}
            <TextField required 
            className="mt-0 w-50"
            name="AccountNumber" 
            label="Account Number (Current ISP)" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.AccountNumber} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.AccountNumber} 
            helperText={!broadbandInfo.AccountNumber ? 'This is required' : ' '}/>

        </React.Fragment>}
        
            {/* if have selected plan is VDSL and not empty */}
        {(props.values.broadbandPlan[0].plan !== '' && props.values.broadbandPlan[0].plan === 'Regular' ) ?
        <React.Fragment>
        {/* if customer selected VDSL and currently does not have an ISP */}
        {(broadbandInfo.haveISP === 'No') && <FormControlLabel control={<Checkbox checked={broadbandInfo.haveJackpoints} onChange={handleChange} name="haveJackpoints" />} 
            label="There are active jackpoints at the property" />}
       {setConnectionDate()}
        </React.Fragment> : (props.values.broadbandPlan[0].plan !== 'Regular') &&
        <React.Fragment>
         {/* if plan is not available and currently does not have an ISP */}
        {(props.values.broadbandPlan[0].plan === "" && broadbandInfo.haveISP === 'No') && <FormControlLabel control={<Checkbox checked={broadbandInfo.haveJackpoints} onChange={handleChange} name="haveJackpoints" />} 
            label="There are active jackpoints at the property" />}
            
         <FormLabel component="legend" style={{marginTop:"1rem"}}>Do you currently have a Fibre Broadband Connection?</FormLabel>
         <RadioGroup row aria-label="haveFibre" label="Do you currently have a Fibre Broadband Connection?" name="haveFibre" value={broadbandInfo.haveFibre} onChange={handleChange}>
             <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
             <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
         </RadioGroup>

         {/* if currently have fibre then set connection date else set scope/installation date */}
         {broadbandInfo.haveFibre === 'Yes'? <React.Fragment>{setConnectionDate()}</React.Fragment> : <React.Fragment>{setScopeDates()}</React.Fragment>}
         </React.Fragment> 
        }
        <legend>Landline Package</legend>
        {/* Landline Package Questions */}
        <FormLabel component="legend" style={{marginTop:"1rem"}}>Would you like to add a Landline Phone Package to your Account?</FormLabel>
         <RadioGroup row aria-label="getLandline" label="Would you like to add a Landline Phone Package to your Account?" name="getLandline" value={broadbandInfo.getLandline} onChange={handleChange}>
             <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
             <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
         </RadioGroup>

         {/* if landline selected */}

         {broadbandInfo.getLandline === 'Yes' && <React.Fragment>
        {/* //  select phone plans */}
        
        <div className="row">{displayPhonePlans()}</div>
        <FormLabel component="legend" style={{marginTop:"1rem"}}>Do you have an existing Landline Phone Number?</FormLabel>
         <RadioGroup row aria-label="haveLandline" label="Do you have an existing Landline Phone Number?" name="haveLandline" value={broadbandInfo.haveLandline} onChange={handleChange}>
             <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
             <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
         </RadioGroup>
         {broadbandInfo.haveLandline === 'Yes' && <React.Fragment>
            <TextField required 
            className="mt-0 w-50"
            name="LandlineNumber" 
            label="Existing Landline Number" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.LandlineNumber} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.LandlineNumber} 
            helperText={!broadbandInfo.LandlineNumber ? 'This is required' : ' '}/>
            <FormLabel component="legend" style={{marginTop:"1rem"}}>Is your current landline with the same company as your internet?</FormLabel>
            <RadioGroup row aria-label="sameLandlineISP" label="Is your current landline with the same company as your internet?" name="sameLandlineISP" value={broadbandInfo.sameLandlineISP} onChange={handleChange}>    
                <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
            </RadioGroup>
            {broadbandInfo.sameLandlineISP === 'No' &&  <React.Fragment>
            <TextField required 
            className="mt-0 w-50"
            name="currentLandlineProvider" 
            label="Current Landline Provider" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.currentLandlineProvider} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.currentLandlineProvider} 
            helperText={!broadbandInfo.currentLandlineProvider ? 'This is required' : ' '}/>
            <TextField required 
            className="mt-0 w-50"
            name="landlineAccountNumber" 
            label="Landline Account Number" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.landlineAccountNumber} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.landlineAccountNumber}  
            helperText={!broadbandInfo.landlineAccountNumber ? 'This is required' : ' '}/>
            <TextField required 
            className="mt-0 w-50"
            name="landlineAccountName" 
            label="Landline Account Name" 
            variant="standard" 
            margin="normal"  
            value={broadbandInfo.landlineAccountName} 
            type="text"
            onChange={handleChange}
            error={!broadbandInfo.landlineAccountName} 
            helperText={!broadbandInfo.landlineAccountName ? 'This is required' : ' '}/>
            </React.Fragment>
         }
            </React.Fragment> 
         }
      

         


         </React.Fragment>
         }

        
         </div>
    )

}