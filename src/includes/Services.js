import React from 'react'
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from "@material-ui/core/TextField";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      maxWidth: 300,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
    formControlLabel:{
      fontWeight:600
    }
  }));
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  const names = [
    'Inside',
    'Outside',
    'Left',
    'Right',
    'Front',
    'Back'
  ];
  
  function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }
export default function Services(props) {
    const classes = useStyles();
    const [services, setState] = React.useState(
      props.values.selectedServices
    );
    const[emptyField, checkField]=React.useState(false);

    const checkThisField=(value)=>{
        if(value !== ''){
            checkField(false)
        }else{
            checkField(true)
        }
    }
  
    const handleChange = (event) => {
      setState({ ...services, [event.target.name]: event.target.checked });
      props.setSelectedServices({...services, [event.target.name]: event.target.checked })
      
    };
  
    const { electricity, gas, lpg, broadband } = services;
    const error = [electricity, gas, lpg,broadband].filter((v) => v).length < 1 ;

  const theme = useTheme();
  const [eMeterLocation, seteMeterLocations] = React.useState(props.values.eMeterLocation);
  const [gasMeterLocation, setGasMeterLocations] = React.useState(props.values.gasMeterLocation);
  const handleMeterLocationChange = (event) => {
    seteMeterLocations(event.target.value);
    props.getElectricityMeterLocations(event.target.value);
  };
  const handleGasMeterLocationChange = (event) => {
    setGasMeterLocations(event.target.value);
    props.getGasMeterLocations(event.target.value);
  };
  

    return (
      <div className={classes.root}>
          <FormGroup>
            <FormControlLabel
              control={<Checkbox checked={electricity} onChange={handleChange} name="electricity" size="medium" style={{fontWeight:"600!important"}}/>}
              label="Electricity" id="checkboxLabel"
        
            />
         <FormLabel component="legend" style={{marginTop:"1rem"}}>Location of your Meter(s)? (Multi select)</FormLabel>
        <Select
          labelId="eMeterLocation"
          id="eMeterLocation"
          multiple
          value={eMeterLocation}
          onChange={handleMeterLocationChange}
          input={<Input />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
          style={{width:"300px"}}
        >
          {names.map((name) => (
            <MenuItem key={name} value={name}>
              <Checkbox checked={eMeterLocation.indexOf(name) > -1} />
              <ListItemText primary={name} />
            </MenuItem>
          ))}
        </Select>
        <FormHelperText style={{fontWeight:"600"}}>Electricity User Type</FormHelperText>
                  <RadioGroup row aria-label="electricityUserType" 
                label="Gas User Type" name="electricityUserType" 
                value={props.values.electricityUserType} 
                onChange={props.handleChange('electricityUserType')} 
                defaultValue={props.values.electricityUserType}
                style={{marginLeft:"2rem"}}>
                    <FormControlLabel value="Standard" control={<Radio color="primary" />} label="Standard" />
                    <FormControlLabel value="Low" control={<Radio color="primary" />} label="Low" />
                </RadioGroup>
        <FormLabel component="legend" style={{marginTop:"1rem"}}>Meter Notes</FormLabel>
                    <TextareaAutosize 
                    aria-label="Meter Notes" 
                    style={{width:"300px",marginBottom:"1rem"}}
                    rowsMin={3} 
                    value={props.values.eMeterNotes} 
                    onChange={props.handleChange('eMeterNotes')}/>

               
                    
            <FormControlLabel
              control={<Checkbox checked={gas} onChange={handleChange} name="gas" />}
              label="Natural gas"
              id="checkboxLabel"
            />
            <FormLabel component="legend" style={{marginTop:"1rem"}}>Location of your Meter(s)? (Multi select)</FormLabel>
        <Select
          labelId="gasMeterLocation"
          id="gasMeterLocation"
          multiple
          label="Location of your Meter(s)? (Multi select)"
          value={gasMeterLocation}
          onChange={handleGasMeterLocationChange}
          input={<Input />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
          style={{width:"300px"}}
        >
          {names.map((name) => (
            <MenuItem key={name} value={name}>
              <Checkbox checked={gasMeterLocation.indexOf(name) > -1} />
              <ListItemText primary={name} />
            </MenuItem>
          ))}
        </Select>
        <FormHelperText style={{fontWeight:"600"}}>Gas User Type (for Powerco only)</FormHelperText>
                <RadioGroup row aria-label="gasUserType" 
                label="Gas User Type (for powerco only)" name="gasUserType" 
                value={props.values.gasUserType} 
                onChange={props.handleChange('gasUserType')} 
                defaultValue={props.values.gasUserType}
                style={{marginLeft:"2rem"}}>
                    <FormControlLabel value="Standard" control={<Radio color="primary" />} label="Standard" />
                    <FormControlLabel value="Low" control={<Radio color="primary" />} label="Low" />
                </RadioGroup>
        <FormLabel component="legend" style={{marginTop:"1rem"}}>Gas Meter Notes</FormLabel>
                    <TextareaAutosize 
                    aria-label="Gas Meter Notes" 
                    style={{width:"300px",marginBottom:"1rem"}}
                    rowsMin={3} 
                    value={props.values.gasMeterNotes} 
                    onChange={props.handleChange('gasMeterNotes')}/>

                <TextField
                    name="gasICP" 
                    label="ICP" 
                    variant="standard"  
                    style={{width:"300px"}}
                    value={props.values.gasICP} 
                    onChange={props.handleChange('gasICP')}
                    inputProps={
                        {maxLength: 15}
                      }
                    helperText="15 digits"/>
                      
            <FormControlLabel
              control={<Checkbox checked={lpg} onChange={handleChange} name="lpg" />}
              label="LPG"
              style={{fontWeight:"600!important"}}
              id="checkboxLabel"
            />
          
                <RadioGroup row aria-label="lpgService" 
                label="LPG" name="lpgService" 
                value={props.values.lpgService} 
                onChange={props.handleChange('lpgService')} 
                defaultValue={props.values.lpgService}
                style={{marginLeft:"2rem"}}>
                    <FormControlLabel value="Yes, sign me up now" control={<Radio color="primary" />} label="Yes, sign me up now" />
                    <FormControlLabel value="I will call in to order my bottles" control={<Radio color="primary" />} label="I will call in to order my bottles" />
                </RadioGroup>
                
          </FormGroup>

          <FormControlLabel
              control={<Checkbox checked={broadband} onChange={handleChange} name="broadband" />}
              label="Unlimited Broadband"
              id="checkboxLabel"
            />
          <FormLabel component="legend" style={{fontWeight:"600"}} className="mt-4 MuiTypography-root MuiFormControlLabel-label">Solar</FormLabel>
                <FormHelperText>Are you interested in hearing more about our solar plans?</FormHelperText>
                <RadioGroup row aria-label="interestInSolar" label="LPG" name="interestInSolar" value={props.values.interestInSolar} onChange={props.handleChange('interestInSolar')} defaultValue={props.values.interestInSolar}>
                    <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                    <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
                </RadioGroup>
                      
      </div>
    );
  }