import React from 'react'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import Grid from '@material-ui/core/Grid';
import validator from 'validator';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker 
} from '@material-ui/pickers';
import TextField from "@material-ui/core/TextField";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

export default function AccountHolderDetails(props) {
    const options = { day: 'numeric', month: 'numeric',year: 'numeric'  };
    const startDate = new Date(new Date().setDate(new Date().getDate()-30));
    const endDate =new Date(new Date().setDate(new Date().getDate()+30));
    const [dob,setBirthDate] = React.useState(props.values.dateOfBirth);
    const [authdob,setauthBirthDate] = React.useState(props.values.authorisedDateOfBirth);
    const [moveInDate,setMoveInDate] = React.useState(props.values.dateOfMoveIn);
    const[confirmEmail, validateConfirmEmail] = React.useState(false);
    const[validEmail, validateEmail] = React.useState(false);
    const DEFAULT_DATE_FORMAT = 'yyyy-MM-dd';
    const[passportExpiry,setPassportExpiry]=React.useState(props.values.passportExpiry);
    const handleBirthDateChange = (date)=>{
        setBirthDate(date)
        props.getBirthDateChange(date);
    };

    const handleAuthorisedBirthDateChange = (date)=>{
        setauthBirthDate(date);
    
        props.getAuthorisedBirthDate(date);
    };
    const handlePassportExpiry =(date)=>{
        setPassportExpiry(date)
        props.getPassportExpiry(date);
    }
    const handleMoveInDateChange = (date)=>{
        setMoveInDate(date);
        // console.log(endDate);
        props.getMoveInDate(date);
    };

    const confirmTwoEmails=()=>{
        if(props.values.confirmEmail !== props.values.email){
            validateConfirmEmail(true)
        }else{
            validateConfirmEmail(false)
        }
    }

    const validateEmailField=()=>{
        if(validator.isEmail(props.values.email)){
            validateEmail(false)
        }else{
            validateEmail(true)
        }
    }

    
  
    return (
        
        <div className="container">
            <div className="row">
           
                <div className="NamesDiv col-12">
                    <FormControl component="fieldset">
                <InputLabel id="titleSelect">Title</InputLabel>
                <Select 
                    required
                    labelId="titleSelect"
                    id="titleSelect"
                    value={props.values.title}
                    onChange={props.handleChange('title')}
                    error={!props.values.title} 
                >
                    <MenuItem value="Mr">Mr</MenuItem>
                    <MenuItem value="Mrs">Mrs</MenuItem>
                    <MenuItem value="Miss">Miss</MenuItem>
                    <MenuItem value="Ms">Ms</MenuItem>
                </Select>
                </FormControl>
                
                    <TextField required
                    name="firstName" 
                    label="First Name" 
                    variant="standard"  
                    value={props.values.firstName} 
                    onChange={props.handleChange('firstName')} 
                    error={!props.values.firstName} 
                    helperText={!props.values.firstName ? 'This is required' : ' '}
                    /> <TextField
                    name="middleName" 
                    label="Middle Name" 
                    variant="standard"  
                    value={props.values.middleName} 
                    onChange={props.handleChange('middleName')} 
                    
                    />
                    <TextField required name="lastName" 
                    label="Last Name" variant="standard"  
                    value={props.values.lastName} 
                    onChange={props.handleChange('lastName')}
                    error={!props.values.lastName} 
                    helperText={!props.values.lastName ? 'This is required' : ' '}
                    />
                </div>
                <div className="col-12" >
                <TextField required name="email" 
                label="Email Address" 
                variant="standard" 
                margin="normal"  
                value={props.values.email.trim()} 
                onChange={props.handleChange('email')} error={validEmail} 
                helperText={validEmail ? 'Please enter a valid email' : ' '}  
                onBlur={validateEmailField} style={{width:"33%"}}/>
                    <TextField id="confirmEmail" onPaste={(e)=>e.preventDefault()} 
                    name="confirmEmail" label="Confirm Email Address" variant="standard" margin="normal" style={{width:"33%"}} error={confirmEmail} 
                    helperText={confirmEmail ? 'Please ensure you\'ve entered the correct email' : ' '} value={props.values.confirmEmail.trim()} onChange={props.handleChange('confirmEmail')} onBlur={confirmTwoEmails} />
                <TextField required 
                name="accountName" 
                style={{width:"70%",marginTop:"0"}}
                label="Account Name" 
                variant="standard" 
                margin="normal"  
                value={props.values.accountName} 
                onChange={props.handleChange('accountName')}
                error={!props.values.accountName} 
                    helperText={!props.values.accountName ? 'This is required' : ' '}/>
                    </div>
                    <div className="col-12" >
                {/* <MuiPickersUtilsProvider utils={MomentUtils}> */}
                        <Grid container justify="flex-start" alignItems="baseline">
                        <KeyboardDatePicker
                    required
                    allowKeyboardControl
                    disableFuture
                    openTo="year"
                    format="dd/MM/yyyy"
                    views={["year", "month", "date"]}
                    value={props.values.dateOfBirth}
                    label="Date Of Birth"
                    onChange={date => handleBirthDateChange(date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                         </Grid>
                            {/* </MuiPickersUtilsProvider>   */}
                            </div>
                               <div className="col-12" >
                            <TextField 
                            name="homePhone" 
                            label="Home Phone" 
                            variant="standard" 
                            margin="normal"  
                            value={props.values.homePhone} 
                            type="number"
                            onChange={props.handleChange('homePhone')}
                            />
                            <TextField required 
                            name="mobilePhone" 
                            label="Mobile" 
                            variant="standard" 
                            margin="normal"  
                            value={props.values.mobilePhone} 
                            type="number"
                            onChange={props.handleChange('mobilePhone')}
                            error={!props.values.mobilePhone} 
                            helperText={!props.values.mobilePhone ? 'This is required' : ' '}/>
                    <TextField name="workPhone" label="Work Phone" variant="standard" margin="normal" type="number" value={props.values.workPhone} onChange={props.handleChange('workPhone')}/>
                  
                        
                    
                </div>
                
                <div className="col-12">
                {/* <FormLabel component="legend" style={{marginTop:"1rem"}}>Have you been billed by another company</FormLabel>
                        <RadioGroup row aria-label="billedByAnotherCompany" label="Have you been billed by another company" name="billedByAnotherCompany" value={props.values.billedByAnotherCompany} onChange={props.handleChange('billedByAnotherCompany')} defaultValue={props.values.billedByAnotherCompany}>
                        <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                        <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
                    </RadioGroup> */}
                <FormLabel component="legend">I am</FormLabel>
                    <RadioGroup row aria-label="customerSituation" label="Are you the account holder for your current power bill" name="customerSituation" value={props.values.customerSituation} onChange={props.handleChange('customerSituation')} defaultValue={props.values.customerSituation}>
                        <FormControlLabel value="Moving to a new house" control={<Radio color="primary" />} label="Moving to a new house" />
                        <FormControlLabel value="Switching from another retailer" control={<Radio color="primary" />} label="Switching from another retailer" />
                    </RadioGroup>
                    {props.values.customerSituation === 'Moving to a new house' ?
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <Grid container justify="flex-start" alignItems="baseline">
                    <KeyboardDatePicker
                    style={{marginBottom:"1rem"}}
                    required
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date of Move in"
                    format="dd/MM/yyyy"
                    value={props.values.dateOfMoveIn}
                    allowKeyboardControl={false}
                    minDate={startDate}
                    maxDate={endDate}
                    onChange={date => handleMoveInDateChange(date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                        </Grid>
                </MuiPickersUtilsProvider>
                    :
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <Grid container justify="flex-start" alignItems="baseline">
                    <KeyboardDatePicker
                    style={{marginBottom:"1rem"}}
                    allowKeyboardControl={false}
                    required
                    margin="normal"
                    id="date-picker-dialog"
                    label="Date of Move in"
                    format="dd/MM/yyyy"
                    value={props.values.dateOfMoveIn}
                    minDate={props.values.dateOfMoveIn}
                    disableFuture
                    onChange={date => handleMoveInDateChange(date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                      
                        </Grid>
                </MuiPickersUtilsProvider>
                    }
                
                    <FormLabel component="legend">Are you the account holder for your current power bill</FormLabel>
                    <RadioGroup row aria-label="isAccountHolder" label="Are you the account holder for your current power bill" name="isAccountHolder" value={props.values.isAccountHolder} onChange={props.handleChange('isAccountHolder')} defaultValue={props.values.isAccountHolder}>
                        <FormControlLabel value="Yes" control={<Radio color="primary" />} label="Yes" />
                        <FormControlLabel value="No" control={<Radio color="primary" />} label="No" />
                    </RadioGroup>
                   
                   
                    <FormLabel component="legend">Access Notes</FormLabel>
                    <TextareaAutosize 
                    aria-label="Access Note" 
                    style={{width:"300px",marginBottom:"1rem"}}
                    rowsMin={3} 
                    value={props.values.accessNote} 
                    onChange={props.handleChange('accessNote')}/>
                    <FormLabel component="legend">Account Comments</FormLabel>
                    <TextareaAutosize 
                    aria-label="Account Comments" 
                    style={{width:"300px",marginBottom:"1rem"}}
                    rowsMin={3} 
                    value={props.values.comments} 
                    onChange={props.handleChange('comments')}/>
                     <FormLabel component="legend"><h6>Identification Details</h6></FormLabel>
                     <FormControl component="fieldset">
                         
                <InputLabel id="identificationType">Type</InputLabel>
                <Select 
                    required
                    labelId="identificationType"
                    id="identificationType"
                    value={props.values.identificationType}
                    onChange={props.handleChange('identificationType')}
                >
                    <MenuItem value="Driver Licence">Driver Licence</MenuItem>
                    <MenuItem value="Passport">Passport</MenuItem>
                </Select>
                </FormControl>
                {props.values.identificationType === 'Driver Licence'
                ?<FormControl component="fieldset"><TextField required 
                    style={{marginTop:"0"}} 
                    name="license5a" 
                    label="Drive Licence 5a" 
                    variant="standard" 
                    margin="normal"  
                    value={props.values.license5a} 
                    onChange={props.handleChange('license5a')}
                    error={!props.values.license5a} 
                    helperText={!props.values.license5a ? 'This is required' : ' '}/>
                    <TextField required
                    style={{marginTop:"0",marginBottom:"1rem"}} 
                    name="license5b" label="Card version 5b" 
                    variant="standard" margin="normal"  
                    value={props.values.license5b} 
                    onChange={props.handleChange('license5b')}
                    error={!props.values.license5b} 
                    helperText={!props.values.license5b ? 'This is required' : ' '}
                    /></FormControl> : 
                    <FormControl component="fieldset">
                        <TextField required 
                    style={{marginTop:"0"}} 
                    name="passportNo" 
                    label="Passport Number" 
                    variant="standard" 
                    margin="normal"  
                    value={props.values.passportNo} 
                    onChange={props.handleChange('passportNo')}
                    error={!props.values.passportNo} 
                    helperText={!props.values.passportNo ? 'This is required' : ' '}/>
                     <TextField required 
                    style={{marginTop:"0"}} 
                    name="passportCountry" 
                    label="Passport Country of Origin" 
                    variant="standard" 
                    margin="normal"  
                    value={props.values.passportCountry} 
                    onChange={props.handleChange('passportCountry')}
                    error={!props.values.passportCountry} 
                    helperText={!props.values.passportCountry ? 'This is required' : ' '}/>
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <Grid container justify="flex-start" alignItems="baseline">
                    <KeyboardDatePicker
                    style={{marginBottom:"1rem"}}
                    required
                    allowKeyboardControl={false}
                    margin="normal"
                    id="date-picker-dialog"
                    label="Passport Expiry Date"
                    format="dd/MM/yyyy"
                    value={props.values.passportExpiry}
                    onChange={date=>handlePassportExpiry(date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                      
                        </Grid>
                </MuiPickersUtilsProvider>
                    </FormControl>}
                    
                    
                    <FormLabel component="legend"><h6>Authorised Contact</h6></FormLabel>
                   </div>
                   
                    <div className="NamesDiv col-12">
                    <FormControl component="fieldset">
                    <InputLabel id="titleSelect">Title</InputLabel>
              
                            <Select 
                            required
                            labelId="authorisedContactTitle"
                            id="authorisedContactTitle"
                            value={props.values.authorisedContactTitle}
                            onChange={props.handleChange('authorisedContactTitle')}
                        > 
                            <MenuItem value="Mr">Mr</MenuItem>
                            <MenuItem value="Mrs">Mrs</MenuItem>
                            <MenuItem value="Miss">Miss</MenuItem>
                            <MenuItem value="Ms">Ms</MenuItem>
                        </Select>
                        </FormControl>
                        <TextField 
                        name="authorisedFirstName" 
                        label="First Name" 
                        variant="standard"  
                        value={props.values.authorisedFirstName} 
                        onChange={props.handleChange('authorisedFirstName')}
                        />
                        <TextField
                        style={{marginBottom:"1rem"}} 
                        name="authorisedLastName" 
                        label="Last Name" 
                        variant="standard"  
                        value={props.values.authorisedLastName} 
                        onChange={props.handleChange('authorisedLastName')}
                        />
                        <Grid container justify="flex-start" alignItems="baseline">
                        <KeyboardDatePicker
                    required
                    allowKeyboardControl
                    disableFuture
                    openTo="year"
                    format="dd/MM/yyyy"
                    views={["year", "month", "date"]}
                    value={props.values.authorisedDateOfBirth}
                    label="Date Of Birth"
                    onChange={date => handleAuthorisedBirthDateChange(date)}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                    />
                         </Grid>
                        
                        
                    
                </div>
                
          
               
        </div>
        </div>
    )
}
