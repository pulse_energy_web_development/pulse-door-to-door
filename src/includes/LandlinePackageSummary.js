import React from 'react'
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import 'react-confirm-alert/src/react-confirm-alert.css'; 
const useStyles = makeStyles({
    root: {
      maxWidth: "100%",
      width:"100%",
      textAlign:'left',
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
      height:"115px"
    },
    content:{
      minHeight:'364px!important',
      borderTop: "3px solid #59a946"
    },
    selectedContent:{
      minHeight:'364px!important',
      borderTop: "3px solid #00A9E2"
    },
    headingSection:{
      display: "flex",
      justifyContent:"space-between",
      flexDirection:"row",
      alignItems: "baseline"
    },
    action:{
      width: '100px',
      justifyContent: "flex-end",
      color:'grey',
      float:'right'
    },
    iconButton:{
      padding: "0!important"
    },
    
    checkCircle:{
      color: "#EBEFEE",
    },
    accordion:{
      border: 'none',
      boxShadow: 'none'
    },
    accordionSummary:{
      width: "66px",
    display: "flex",
    justifyContent: "flex-end",
    flexDirection: "row",
    alignItems: "flex-end"
    },
    expandIcon:{
      padding:'0!important'
    },
    icon: {
      borderRadius: '50%',
      width: 22,
      height: 22,
      boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
      backgroundColor: '#f5f8fa',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
      '$root.Mui-focusVisible &': {
        outline: '2px auto rgba(19,124,189,.6)',
        outlineOffset: 2,
      },
      'input:hover ~ &': {
        backgroundColor: '#00a9e2',
      },
      'input:disabled ~ &': {
        boxShadow: 'none',
        background: 'rgba(206,217,224,.5)',
      },
    },
    checkedIcon: {
      backgroundColor: 'transparent',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
      '&:before': {
        display: 'block',
        width: 22,
        height: 22,
        backgroundImage: "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMjsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggc3R5bGU9ImZpbGw6IzFFOTBGRjsiIGQ9Ik0yMjUuODgyLDMzMS4yOTRjLTcuOTgxLTAuMDMtMTUuNjE2LTMuMjIzLTIxLjIzMy04Ljg4NWwtNDUuMTc2LTQ1LjE3Ng0KCWMtMTEuNzYxLTExLjc2MS0xMS43NjEtMzAuODU2LDAtNDIuNjE2czMwLjg1Ni0xMS43NjEsNDIuNjE2LDBsMjUuNDQ5LDI1LjQ0OWw4NC4xNzktNzIuMTMyDQoJYzEzLjgwOS05LjI3NiwzMi41MjctNS41ODcsNDEuNzg4LDguMjIyYzcuNjk1LDExLjQ3NSw2LjYxMSwyNi42OTktMi42MzUsMzYuOTU0bC0xMDUuNDEyLDkwLjM1Mw0KCUMyNDAuMDgzLDMyOC4zNDMsMjMzLjE0MSwzMzEuMTI4LDIyNS44ODIsMzMxLjI5NHoiLz4NCjxwYXRoIHN0eWxlPSJmaWxsOiMxNjZDQkY7IiBkPSJNMjU2LDUxMkMxMTQuNjEzLDUxMiwwLDM5Ny4zODcsMCwyNTZTMTE0LjYxMywwLDI1NiwwczI1NiwxMTQuNjEzLDI1NiwyNTZTMzk3LjM4Nyw1MTIsMjU2LDUxMnoNCgkgTTI1Niw2MC4yMzVDMTQ3Ljg3OCw2MC4yMzUsNjAuMjM1LDE0Ny44NzgsNjAuMjM1LDI1NlMxNDcuODc4LDQ1MS43NjUsMjU2LDQ1MS43NjVTNDUxLjc2NSwzNjQuMTIyLDQ1MS43NjUsMjU2DQoJUzM2NC4xMjIsNjAuMjM1LDI1Niw2MC4yMzV6Ii8+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg==)",
        content: '""',
      },
      'input:hover ~ &': {
        backgroundColor: '#00a9e2',
      },
    },
    availableText :{
      color:'#00a9e2',
      fontSize:"1.3rem",
      fontWeight:"600"
      
  }
  });

export default function LandlinePackageSummary(props) {
    const classes = useStyles();
    // const [show, toggleShow]=React.useState(false)
    const [selected, setSelected] = React.useState(false)
    // const [disable, setDisable] = React.useState(false)
    
    function handleRemove(title) {
      const newList = props.confirmPhonePlan.filter((item) => item.title !== title);
      //save new phone plan array
      props.setPhonePlan(newList)
    }
   
    const handleOptionChange=(plan,details,price)=>{

      let item =  {'title':plan,
      'details':details,
      'price': price}
      let check = itemInArray(item);
      let currentPhonePlan = props.confirmPhonePlan;
      console.log(check)
      //when internation in confirmphoneplan array then remove the addon
      if(check){
        setSelected(false)
        if(currentPhonePlan.some(phonePlan => phonePlan.title === item.title)){
           if(item.title !== 'Local'){
            handleRemove(item.title)
          }
        
        }
      }else{
        setSelected(true)
        // save new add on as object to array
        props.setPhonePlan(item)
      }
      
    }
    //check if obj in array
    const itemInArray =(item)=>{
      let currentPhonePlan = props.confirmPhonePlan;
    
      if(currentPhonePlan.some(phonePlan => phonePlan.title === item.title)){
        currentPhonePlan.some(phonePlan => console.log( phonePlan.title))
        // "Object found inside the array."
        return true
    } else{
        currentPhonePlan.some(phonePlan => console.log( phonePlan.title))
        // alert("Object not found."
        return false
    }
    }


    return (
      // 0000120411TR376
      <div className="col-xs-12 col-md-6 col-lg-6 d-flex justify-content-center align-items-start phone p-1" key={props.index}>   
        <Card className={classes.root} >
        <CardActionArea onClick={(e)=>handleOptionChange(props.obj.title,props.obj.details,props.obj.price)}>
        <CardContent className={props.defaultPlanTitle === props.obj.title || selected ? 
                  classes.selectedContent :
                  classes.content} >
          <Typography variant="h4" component="h2" className={classes.headingSection}>
          {props.obj.title} 
          
         {props.defaultPlanTitle === props.obj.title || selected ?<span className={clsx(classes.icon, classes.checkedIcon)} /> :<RadioButtonUncheckedIcon/>}
          </Typography>
          <br/>
          <Typography className={classes.pos} color="textSecondary">
          <div dangerouslySetInnerHTML={{__html: props.obj.details}} ></div>
          </Typography>
          
          <Typography> <span style={{fontSize:'4rem'}}>{props.obj.price}</span> <b>/month +GST</b></Typography>
          
        </CardContent>
        </CardActionArea>
     </Card>
     </div>

    )
}
