import React from 'react';
import SignaturePad from "react-signature-canvas";
import TextField from "@material-ui/core/TextField";
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

export default function KeyTerms(props) {
    const [imageURL, setImageURL] = React.useState(null); // create a state that will contain our image url
    const [termsHTMl] = React.useState(props.values.terms);
    const [broadbandTermsHTMl] = React.useState(props.values.broadbandTerms);
    const sigCanvas = React.useRef({});
    /* a function that uses the canvas ref to clear the canvas 
  via a method given by react-signature-canvas */
  const clear = () => sigCanvas.current.clear();

  /* a function that uses the canvas ref to trim the canvas 
  from white spaces via a method given by react-signature-canvas
  then saves it in our state */
  const save = () => {
    props.getSignature(sigCanvas.current.getTrimmedCanvas().toDataURL("image/png"));
    setImageURL(sigCanvas.current.getTrimmedCanvas().toDataURL("image/png"));
  }
 
    return (
        <div className="keyTermsDiv">
          {props.values.selectedServices.broadband} 
            <div dangerouslySetInnerHTML={{__html: termsHTMl}}></div>
            {props.values.selectedServices.broadband && <div dangerouslySetInnerHTML={{__html: broadbandTermsHTMl}}></div>}
            <p>Pulse Energy is a trading name of Pulse Energy Alliance LP and you will be registered as a Pulse Energy customer with the Electricity Authority.</p>
            <p><strong>By signing below you confirm that you wish to proceed as a customer of Pulse Energy, and that you authorise us to arrange the switch with your current supplier.</strong></p>
            <div className="SignaturePad text-center">
               <SignaturePad
                ref={sigCanvas}
                canvasProps={{
                    width:500,
                    height:200,
                    clearOnResize:false,
                  className: "signatureCanvas"
                }}
                />
            </div>

            <div className="text-center">
            <button className="MuiButtonBase-root MuiButton-root MuiButton-contained jss2 MuiButton-containedPrimary" onClick={save}><span className="MuiButton-label">Save</span></button>
            <button className="MuiButtonBase-root MuiButton-root MuiButton-contained jss2 MuiButton-containedPrimary" onClick={clear}><span className="MuiButton-label">Clear</span></button>
            </div>
            
                    <FormControl component="fieldset">
            <InputLabel id="titleSelect">Company</InputLabel>
                <Select 
                    required
                    labelId="Company"
                    id="Company"
                    value={props.values.company}
                    onChange={props.handleChange('company')}
                    error={!props.values.company} 
                    style={{minWidth:"100px"}}
                >
                    <MenuItem value="Focus">Focus Marketing</MenuItem>
                </Select>
                </FormControl>
                <TextField
                    required
                    name="representativeName" 
                    label="Pulse Energy Representative Name" 
                    variant="standard"  
                    value={props.values.representativeName} 
                    onChange={props.handleChange('representativeName')}
                    error={!props.values.representativeName} 
                    helperText={!props.values.representativeName ? 'This is required' : ' '}  
                    
                   style={{width:"320px"}}
                    />
        </div>
    )
}
