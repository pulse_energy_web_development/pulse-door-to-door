const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports= function(app){
  const { NODE_ENV } = process.env;
  const proxyTarget = "http://localhost/online_signup/";
  if(NODE_ENV === "development") {
     app.use(
      '/door-2-door/getBroadbandPlans/',
      createProxyMiddleware({
        target: proxyTarget,
        changeOrigin: true,
      })
    );
    app.use(
      '/door-2-door/DtoDsignupFormHandler',
      createProxyMiddleware({
        target: proxyTarget,
        changeOrigin: true,
      })
    );
    
    app.use(
      '/door-2-door/getDataFromBackend',
      createProxyMiddleware({
        target: proxyTarget,
        changeOrigin: true,
      })
    );
  }else{
    app.use(
      '/online_signup/home/callPriceCalculator/',
      createProxyMiddleware({
        target: "http://dwh-prdapp-01-dc.pulse.local/GTPricingCalculatorWS/PricingCalculator.svc?wsdl",
        changeOrigin: true,
        pathRewrite: {'^/online_signup/home/callPriceCalculator' : ''},
      })
    );
  }
  
};